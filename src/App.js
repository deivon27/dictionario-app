import React, {Component} from 'react';

import {Route, Switch, withRouter} from 'react-router-dom'

import {
    Navbar, NavbarBrand
} from 'reactstrap';

/** Import the needed components for React Router **/
import Dictionaries from './components/Dictionaries';
import AddDictionary from "./components/AddDictionary";
import EditDictionary from "./components/EditDictionary";

/** Wrapper for withRouter **/
const WrappedApp = withRouter(props => <App {...props}/>);

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpenNavBar: false
        };
    }

    render() {
        return (
            <div className="App">
                <Navbar color="light" light expand="md">
                    <NavbarBrand href="/">Dictionario App</NavbarBrand>
                </Navbar>

                <Switch>
                    <Route path="/" component={Dictionaries} exact/>
                    <Route path="/addDictionary" component={AddDictionary}/>
                    <Route path="/editDictionary/:id" component={EditDictionary} />
                </Switch>
            </div>
        )
    };
}

export default WrappedApp;