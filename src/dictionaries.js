// Sample data dictionaries
export default [
    {
        id: 1,
        name: "dictionary1",
        dictionary: [
            {id: 1, domain: "Stonegrey", range: "Dark Grey"},
            {id: 2, domain: "Stonegrey", range: "Dark Grey"},
            {id: 3, domain: "Caribbean Sea", range: "Turquoise"}
        ],
        validationErrors: []
    },
    {
        id: 2,
        name: "dictionary2",
        dictionary: [
            {id: 1, domain: "Stonegrey", range: "Dark Grey"},
            {id: 2, domain: "Stonegrey", range: "Anthracite"},
            {id: 3, domain: "Midnight Blue", range: "Dark Blue"}
        ],
        validationErrors: []
    }
    ,
    {
        id: 3,
        name: "dictionary3",
        dictionary: [
            {id: 1, domain: "Stonegrey", range: "Dark Grey"},
            {id: 2, domain: "Dark Grey", range: "Stonegrey"},
            {id: 3, domain: "Midnight Blue", range: "Dark Blue"}
        ],
        validationErrors: []
    },
    {
        id: 4,
        name: "dictionary4",
        dictionary: [
            {id: 1, domain: "Stonegrey", range: "Dark Grey"},
            {id: 2, domain: "Dark Grey", range: "Anthracite"},
            {id: 3, domain: "Midnight Blue", range: "Dark Blue"}
        ],
        validationErrors: []
    }
]