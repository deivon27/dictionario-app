import React, {Component} from 'react';
import {Table, Button, Modal, ModalHeader, ModalBody, ModalFooter, Badge} from 'reactstrap';
import {Link} from "react-router-dom";

export class DictionaryRow extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modalRemovePrompt: false,
            modalViewDict: false
        }
    }


    /** Remove dictionary prompt **/
    onToggleRemovePrompt = () => {
        this.setState(prevState => ({
            modalRemovePrompt: !prevState.modalRemovePrompt
        }));
    };


    /** View dictionary prompt **/
    onToggleViewDict = () => {
        this.setState(prevState => ({
            modalViewDict: !prevState.modalViewDict
        }));
    };


    /**
     * Hotfix for forced closing of 'remove dictionary prompt' modal
     * @param id
     * @private
     */
    _fixRemoveModal = (id) => {
        this.props.deleteDictionary(id);
        this.onToggleRemovePrompt();
    };


    /**
     * Render only the dictionaly table
     * @param dict
     * @returns {*}
     */
    renderTableDictionary = (dict) => {
        return (
            <Table size="sm" striped>
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Domain</th>
                    <th>Range</th>
                </tr>
                </thead>
                <tbody>
                {
                    dict.map((item, key) => {
                        return (
                            <tr key={key}>
                                <td>{item.id}</td>
                                <td>{item.domain}</td>
                                <td>{item.range}</td>
                            </tr>
                        )
                    })
                }
                </tbody>
            </Table>
        )
    };


    /** Render error markers on every dictionary row **/
    renderErrorBadges = () => {
        let badges = [];
        let errs = this.props.errorsMarkers;

        if (errs && errs.length > 0) {
            let uniqueErrs = [...new Set(errs)];
            for (let i = 0; i <= uniqueErrs.length; i++) {
                switch (uniqueErrs[i]) {
                    case 'duplicates' :
                        badges.push({color: "primary", value: uniqueErrs[i]});
                        break;
                    case 'forks' :
                        badges.push({color: "info", value: uniqueErrs[i]});
                        break;
                    case 'cycles' :
                        badges.push({color: "warning", value: uniqueErrs[i]});
                        break;
                    case 'chains' :
                        badges.push({color: "danger", value: uniqueErrs[i]});
                        break;
                    case 'default':
                        return true;
                }
            }

            return badges.map((item, i) => <Badge key={i} color={item.color} pill>{item.value}</Badge>);
        }
    };

    render() {
        const {id, dict, name, errorsMarkers} = this.props;

        return (
            <tr key={id}>
                <th scope="row">{id}</th>
                <td>{name}</td>

                {/* Render error markers on every dictionary row */}
                <td>{this.renderTableDictionary(dict)}</td>

                {/* Render error markers on every dictionary row */}
                <td>
                    {(errorsMarkers && errorsMarkers.length > 0) && this.renderErrorBadges()}
                </td>
                <td>

                    {/* Action buttons */}
                    <Button color="primary" className="btn-margin action"
                            onClick={() => this.props.executeAllValidations(dict, id)}>Validate</Button>
                    <Button color="secondary" className="btn-margin action"
                            onClick={() => this.onToggleViewDict(dict)}>View</Button>

                    {/* Modal for view single dictionary */}
                    <Modal isOpen={this.state.modalViewDict} toggle={this.onToggleViewDict}>
                        <ModalHeader toggle={this.onToggleViewDict}>{name}</ModalHeader>
                        <ModalBody>
                            {this.renderTableDictionary(dict)}
                        </ModalBody>
                    </Modal>

                    {/* Edit dictionary button */}
                    <Link to={`/editDictionary/${id}`}>
                        <Button color="warning" className="btn-margin action">Edit</Button>
                    </Link>

                    {/* Remove dictionary button */}
                    <Button color="danger" className="btn-margin action"
                            onClick={this.onToggleRemovePrompt}>Delete</Button>


                    {/* Modal prompt for remove a dictionary */}
                    <Modal isOpen={this.state.modalRemovePrompt} toggle={this.onToggleRemovePrompt}>
                        <ModalHeader toggle={this.onToggleRemovePrompt}>Removing dictionary</ModalHeader>
                        <ModalBody>
                            Are you sure to cancel this dictionary?
                        </ModalBody>
                        <ModalFooter>
                            <Button color="danger" onClick={() => this._fixRemoveModal(id)}>Remove</Button>{' '}
                            <Button color="secondary" onClick={this.onToggleRemovePrompt}>Cancel</Button>
                        </ModalFooter>
                    </Modal>
                </td>
            </tr>
        )
    }
}