import React, {Component} from 'react';
import {Table, Button, UncontrolledAlert, Alert} from 'reactstrap';
import {Link} from "react-router-dom";


/** Assign this value 'dictionaries' on 24 line if you want to test asap with some sample data **/
import dictionaries from '../dictionaries';

import {DictionaryRow} from './DictionaryRow';


export default class Dictionaries extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            validationAlertVisible: false,
            dictionaries: [],
            validationTexts: [],
            errorsMarkers: [],
            currentIdErrors: null
        };

        this.dictionaries = !localStorage ? dictionaries : this.getAllStorage();
        this.errorsMarkers = []
    }


    /**
     * Create new object in array of dictionary table objects
     * @returns {Array}
     */
    getAllStorage = () => {
        let values = [],
            keys = Object.keys(localStorage),
            i = keys.length;

        while (i--) {
            values.push(JSON.parse(localStorage.getItem(keys[i])));
        }
        return values;
    };


    /**
     * Load the dictionaries available from localStorage to the state
     */
    loadStateFromStorage = () => {
        let dictionaries = this.getAllStorage();
        this.setState({
            dictionaries: dictionaries
        })
    };


    /**
     * Remove dictionary
     * @param id
     */
    deleteDictionary = (id) => {
        localStorage.removeItem(`${id}`);
        this.loadStateFromStorage();
    };


    /**
     * Show validation alerts
     * @param text
     */
    showValidationText = (text) => {
        this.setState(prevState => ({
            validationTexts: [...prevState.validationTexts, text],
            validationAlertVisible: true
        }));
    };


    /**
     * Execute all the available validations on the certain dictionary
     * @param arr
     * @param id
     */
    executeAllValidations = (arr, id) => {
        this.errorsMarkers = [];
        this.setState({
            validationTexts: [],
            errorsMarkers: [],
            currentIdErrors: id
        }, () => this.getDuplicates(arr));
    };


    /**
     * Execute all the validations on the certain dictionary
     * @param id
     * @param itemAttributes
     */
    updateDictionaryStateFromLocal(id, itemAttributes) {
        let index = this.state.dictionaries.findIndex(x => x.id === id);
        if (index !== -1) {
            this.setState({
                dictionaries: [
                    ...this.state.dictionaries.slice(0, index),
                    Object.assign({}, this.state.dictionaries[index], itemAttributes),
                    ...this.state.dictionaries.slice(index + 1)
                ]
            });
        }
    }


    /**
     * Update validation error markers asynchronously on localStorage and state
     */
    setErrorsMarkers = () => {
        if (this.errorsMarkers.length > 0) {
            let id = this.state.currentIdErrors;

            /* Get the validation errors array from the localStorage */
            let dictionaryToEdit = JSON.parse(localStorage.getItem(`${id}`));

            /* Update the validation errors array with the new ones */
            dictionaryToEdit['validationErrors'] = this.errorsMarkers;

            /* Resave the validation errors array to the localStorage */
            localStorage.setItem(`${id}`, JSON.stringify(dictionaryToEdit));

            /* Update the state with the new validation errors */
            if (this.state.dictionaries.length > 0) {
                this.updateDictionaryStateFromLocal(id, {validationErrors: this.errorsMarkers});
            }
        }

        /* Scroll to top of the browser window to see the validation errors */
        window.scrollTo(0, 0);
    };


    /**
     * Search for a duplicates in a dictionary
     * @param arr
     */
    getDuplicates = (arr) => {
        if (arr.length <= 1) {
            alert("No validation could be done on one row!")
        } else {
            const duplicates = arr.map(e => e['domain'] && e['range'])
                .map((e, i, final) => final.indexOf(e) !== i && i)
                .filter(e => arr[e]).map(e => arr[e]);

            this.showValidationText(duplicates.length > 0 ? `Duplicates error: Row ${duplicates[0].id} is duplicated with another one` : "No duplicates errors found");
            if (duplicates.length > 0) this.errorsMarkers.push('duplicates');
            this.setErrorsMarkers();

        }
        this.getForks(arr, 'domain');
    };


    /**
     * Search for a forks in a dictionary
     * @param arr
     * @param key
     */
    getForks = (arr, key) => {
        if (arr.length <= 1) {
            alert("No validation could be done on one row!");
        } else {
            const forks = arr.map(e => e[key])
                .map((e, i, final) => final.indexOf(e) !== i && i)
                .filter(e => arr[e]).map(e => arr[e]);

            this.showValidationText(forks.length > 0 ? `Forks error: Domain ${forks[0].domain} duplicated` : "No forks errors found!");
            if (forks.length > 0) this.errorsMarkers.push('forks');
            this.setErrorsMarkers();

        }
        this.getCycles(arr);
    };


    /**
     * Search for a cycles in a dictionary
     * @param arr
     */
    getCycles = (arr) => {
        if (arr.length <= 1) {
            alert("No validation could be done on one row!");
        } else {
            let errors = "";

            arr.map((item, index, self) => {
                    let currId = self[index].id,
                        currDomain = self[index].domain,
                        currRange = self[index].range,
                        nextDomain = self[index + 1] && self[index + 1].domain,
                        prevDomain = self[index - 1] && self[index - 1].domain,
                        nextRange = self[index + 1] && self[index + 1].range,
                        prevRange = self[index - 1] && self[index - 1].range;

                    try {
                        if (index + 1 < self.length && (currDomain === nextRange)) {
                            errors += `Cycles error: Domain on row ${currId} "${currDomain}" is cycled with range on the next row: "${nextRange}"<br/>`;
                        } else if (index > 0 && (currDomain === prevRange)) {
                            errors += `Cycles error: Domain on row ${currId}  "${currDomain}" is cycled with range on the previous row: "${prevRange}"<br>`;
                        } else if (index + 1 < self.length && (currRange === nextDomain)) {
                            errors += `Cycles error: Range on row ${currId}  "${currDomain}" is cycled with domain on the next row: "${nextDomain}"<br>`;
                        } else if (index > 0 && (currRange === prevDomain)) {
                            errors += `Cycles error: Range on row ${currId}  "${currRange}" is cycled with domain on the previous row: "${prevDomain}"<br>`;
                        }
                    } catch (e) {
                        //console.log(e);
                        /** TO-DO: FIX -- TypeErrors **/
                    }
                }
            );

            if (errors) {
                this.showValidationText(errors);
                this.errorsMarkers.push('cycles');
                this.setErrorsMarkers();
            } else {
                this.showValidationText("No cycles errors found!");
            }
        }
        this.getChains(arr);
    };


    /**
     * Search for a chains in a dictionary
     * @param arr
     */

    getChains = (arr) => {
        if (arr.length <= 1) {
            alert("No validation could be done on one row!");
        } else {
            let errors = "";

            arr.some((item, index, self) => {
                    try {
                        if ((self[index].range === self[index + 1].domain)) {
                            errors += `Chains error: Range on row ${self[index].id} "${self[index].range}" is chained with domain on the next row: "${self[index + 1].domain}"<br>`;
                        }
                    } catch (e) {
                        //console.log(e);
                        /** TO-DO: FIX -- TypeErrors **/
                    }
                }
            );

            if (errors) {
                this.showValidationText(errors);
                this.errorsMarkers.push('chains');
                this.setErrorsMarkers();
            } else {
                this.showValidationText("No chains errors found!");
            }
        }
    };

    onDismissValidationAlert = () => {
        this.setState({validationAlertVisible: false});
    };

    componentWillMount() {
        /** Populate localStorage from local dictionary array row by row **/
        this.dictionaries.map((data) => {
            localStorage.setItem(`${data.id}`, JSON.stringify(data))
        })
    }

    componentDidMount() {
        /** Load the dictionaries available from localStorage to the state **/
        this.loadStateFromStorage();
    }

    render() {

        const {dictionaries} = this.state;

        return (
            <>
                {/* Show all the validation errors after 'Validate' button click */}
                {
                    this.state.validationTexts.map((row, i) => {
                        return (
                            <UncontrolledAlert key={i} color="warning" isOpen={this.state.validationAlertVisible}
                                               toggle={this.onDismissValidationAlert}>
                                <div dangerouslySetInnerHTML={{__html: row}}/>
                            </UncontrolledAlert>
                        )
                    })
                }

                {/* Add new dictionary button */}
                <div className="centered">
                    <Link to="/addDictionary"><Button color="info" className="margin-btn">Add dictionary</Button></Link>
                </div>

                {/* Main list with all dictionaries available */}
                <Table>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Content</th>
                        <th>Errors</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        dictionaries.length > 0 ?
                            dictionaries.map((data, i) => {
                                return <DictionaryRow id={data.id}
                                                      key={i}
                                                      dict={data.dictionary}
                                                      name={data.name}
                                                      errorsMarkers={data.validationErrors}
                                                      executeAllValidations={this.executeAllValidations}
                                                      deleteDictionary={(id) => this.deleteDictionary(id)}/>
                            }) :

                            <tr>
                                {/* In case there are no dictionaries available */}
                                <td colSpan={5}>
                                    <Alert color="info" className="centered">
                                        <div>No dictionaries found, please add a new one!</div>
                                    </Alert>
                                </td>
                            </tr>
                    }
                    </tbody>
                </Table>
            </>
        );
    }
}