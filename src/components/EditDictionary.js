import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';
import {
    Button,
    FormGroup,
    Label,
    Input,
    Form,
    Alert,
    Breadcrumb,
    BreadcrumbItem, Card
} from 'reactstrap';
import {EditDictionaryContentRow} from './EditDictionaryContentRow';


class EditDictionary extends Component {

    constructor(props) {
        super(props);

        this.state = {
            dictName: '',
            dictionaryRows: [],
            alertIsVisible: false,
            dictionaryToEdit: null,
            dictionaryValidationErrors: null,
            removedRowId: ''
        };

        this.dictionaryRows = []
    }

    /**
     * Create new empty row
     */
    addNewRow = () => {
        let dictId = parseInt(`${Math.round(Math.random() * 100)}`);
        this.setState(prevState => ({
            dictionaryRows: [...prevState.dictionaryRows, {
                id: dictId,
                domain: '',
                range: ''
            }]
        }));
    };


    /**
     * Remove existent row
     * @param id
     */
    removeDictionaryRow = (id) => {
        const dictionaries = this.state.dictionaryRows.filter(item => parseInt(item.id) !== parseInt(id));
        this.setState({
            dictionaryRows: dictionaries,
            removedRowId: id
        });
    };


    /**
     * Update the dictionary table row
     * @param newRow
     */
    updateDictionary = (newRow) => {

        this.dictionaryRows = this.state.dictionaryRows;

        const index = this.dictionaryRows.findIndex((e) => e.id === newRow.id);
        if (index === -1) {
            this.dictionaryRows.push(newRow);
        } else {
            this.dictionaryRows[index] = newRow;
        }

        this.setState({
            dictionaryRows: this.dictionaryRows
        });
    };


    /**
     * Save the dictionary table, updating the main array with dictionaries
     */
    saveDictionary = () => {
        if (!this.state.dictName || this.state.dictionaryRows.length === 0) {
            this.setState({
                alertIsVisible: true
            });
        } else {
            let dictId = this.props.match.params.id;

            let dictionaryComplete = {
                id: dictId,
                name: this.state.dictName,
                dictionary: this.state.dictionaryRows,
                validatingStatus: this.state.validatingStatus
            };

            localStorage.setItem(`${dictId}`, JSON.stringify(dictionaryComplete));
            this.props.history.push('/')
        }
    };


    onChangeName = (e) => {
        this.setState({dictName: e.target.value});
        this.setState({
            alertIsVisible: !this.state.dictName
        });
    };


    /**
     * Load an actual dictionary
     */
    componentWillMount() {
        let id = this.props.match.params.id;

        let dictionaryToEdit = JSON.parse(localStorage.getItem(`${id}`));
        this.setState({
            dictionaryToEdit: dictionaryToEdit,
            dictName: dictionaryToEdit.name,
            dictionaryRows: dictionaryToEdit.dictionary,
            dictionaryValidationErrors: dictionaryToEdit.validationErrors
        });
    }

    render() {

        const {dictionaryRows} = this.state;

        /** Set the name of the required fields in the alert **/
        let alertText = "";
        if (!this.state.dictName) {
            alertText = "Dictionary name";
        } else if (this.state.dictionaryRows.length === 0) {
            alertText = "Dictionary row";
        }


        return (
            <div className="container-fluid wrapper">

                {/* Breadcrumbs for navigation */}
                <Breadcrumb>
                    <BreadcrumbItem><Link to="/">Dictionaries</Link></BreadcrumbItem>
                    <BreadcrumbItem active>Edit dictionary</BreadcrumbItem>
                </Breadcrumb>

                {/* Alert for required fields errors */}
                <Alert color="danger" isOpen={this.state.alertIsVisible}>
                    {`Please fill in the required field "${alertText}"`}
                </Alert>

                {/* Main form to edit dictionary data */}
                <Card body>
                    <Form>
                        <FormGroup>
                            <Label for="name">Dictionary name</Label>
                            <Input type="text"
                                   name="name"
                                   id="name"
                                   required
                                   placeholder="e.g. T-Shirt colors"
                                   value={this.state.dictName}
                                   onChange={this.onChangeName}/>
                        </FormGroup>
                        <div>Edit dictionary, adding the new rows as well</div>

                        {/* Set the name of the required fields in the alert */}
                        <Button outline color="primary" className="btn-margin" onClick={this.addNewRow}>+ New
                            dictionary row</Button>

                        {/* Show the dictionary table rows */}
                        {this.state.dictionaryRows.length > 0 && (
                            this.state.dictionaryRows.map((dict, i) => {
                                return (this.state.removedRowId !== dict.id) &&
                                    <EditDictionaryContentRow key={i} id={dict.id}
                                                              domain={dict.domain}
                                                              range={dict.range}
                                                              removedRowId={this.state.removedRowId}
                                                              dictionaryRow={[dict]}
                                                              updateDictionary={(v) => this.updateDictionary(v)}
                                                              removeDictionaryRow={this.removeDictionaryRow}/>
                            })
                        )}
                        {
                            dictionaryRows.length > 0 &&
                            <>
                                <Button color="success"
                                        className="btn-margin"
                                        onClick={this.saveDictionary}>Save and Go Back</Button>
                            </>
                        }
                    </Form>
                </Card>
            </div>
        )
    }
}

export default withRouter(EditDictionary)