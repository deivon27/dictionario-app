import React, {Component} from 'react';
import {
    Button,
    Input,
    Col,
    Row
} from 'reactstrap';

export class AddDictionaryContentRow extends Component {

    constructor(props) {
        super(props);

        this.state = {
            dictionaryRow: [],
            iterableDomainValue: '',
            iterableRangeValue: ''
        };
    }


    /**
     * Create a new dictionary table row
     * @param rowId
     */
    addNewDictObject = (rowId) => {
        this.setState(prevState => ({
            dictionaryRow: [...prevState.dictionaryRow, {
                id: rowId,
                domain: this.state.iterableDomainValue,
                range: this.state.iterableRangeValue
            }]
        }), () => {
            /** Update the main dictionary table array in the parent (callback method) **/
            this.props.updateDictionary(this.state.dictionaryRow[0]);
        });
    };


    /**
     * Update the dictionary table row
     * @param rowToUpdate
     */
    updateDictionaryRow = (rowToUpdate) => {

        /** Id of iterable row **/
        let rowId = this.props.id;

        /** if current dictionary row exists already **/
        if (this.state.dictionaryRow.length > 0) {

            const isExistRow = this.state.dictionaryRow.some(v => v.id === rowId);

            /** if the iterable row exist in the dictionary **/
            if (!isExistRow) this.addNewDictObject(rowId, rowToUpdate);

            /** ...if exist **/
            else {

                /** Immutable change of dictionaryRow object properties **/
                this.setState(prevState => {
                    let newDictionary = Object.assign({}, prevState.dictionaryRow);
                    newDictionary[0].domain = this.state.iterableDomainValue;
                    newDictionary[0].range = this.state.iterableRangeValue;
                    return {newDictionary};
                }, () => {
                    /** Update the main dictionary table array in the parent (callback method) **/
                    this.props.updateDictionary(this.state.dictionaryRow[0]);
                });
            }
        }

        /** if current dictionary row not exists **/
        else {
            this.addNewDictObject(rowId, rowToUpdate);
        }
    };

    onChangeDomain = (e) => {
        this.setState({iterableDomainValue: e.target.value});
    };

    onChangeRange = (e) => {
        this.setState({iterableRangeValue: e.target.value});
    };

    render() {
        const {id} = this.props;

        return (
            <Row form>
                <Col md={5}>
                    <Input type="text"
                           required
                           className="input-margin"
                           name={`domain${id}`}
                           placeholder="Domain"
                           value={this.state.iterableDomainValue}
                           onChange={this.onChangeDomain}
                           onKeyUp={() => this.updateDictionaryRow()}/>
                </Col>
                <Col md={5}>
                    <Input type="text"
                           required
                           className="input-margin"
                           name={`range${id}`}
                           placeholder="Range"
                           value={this.state.iterableRangeValue}
                           onChange={this.onChangeRange}
                           onKeyUp={() => this.updateDictionaryRow()}/>
                </Col>
                <Col md={2}>
                    <Button outline color="danger"
                            className="btn-block btn-margin"
                            onClick={() => this.props.removeDictionaryRow(this.props.id)}>x</Button>
                </Col>
            </Row>
        )
    }
}