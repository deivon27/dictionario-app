import React, {Component} from 'react';
import {
    Button,
    FormGroup,
    Label,
    Input,
    Form,
    Alert,
    Breadcrumb,
    BreadcrumbItem, Card
} from 'reactstrap';
import {AddDictionaryContentRow} from './AddDictionaryContentRow'
import {Link, withRouter} from 'react-router-dom'


class AddDictionary extends Component {

    constructor(props) {
        super(props);

        this.state = {
            countDictRows: 1,
            dictName: '',
            dictionaryRows: [],
            alertIsVisible: false,
            removedRowId: ''
        };

        this.dictionaryRows = []
    }

    /**
     * Create new empty row
     */
    addNewRow = () => {
        this.setState((prevState) => {
            return {countDictRows: prevState.countDictRows + 1}
        })
    };


    /**
     * Remove existent row
     * @param id
     */
    removeDictionaryRow = (id) => {
        const dictionaries = this.state.dictionaryRows.filter(item => item.id !== id);
        this.setState({
            dictionaryRows: dictionaries,
            removedRowId: id
        });
    };


    /**
     * Update the dictionary table
     * @param newRow
     */
    updateDictionary = (newRow) => {

        /** if existing array is empty **/
        if (this.state.dictionaryRows.length === 0) {
            this.dictionaryRows.push(newRow);
        }

        /** if existing array is NOT empty **/
        else {
            const index = this.dictionaryRows.findIndex((e) => e.id === newRow.id);
            if (index === -1) {
                this.dictionaryRows.push(newRow);
            } else {
                this.dictionaryRows[index] = newRow;
            }
        }

        /** Update the dictionary table array **/
        this.setState({
            dictionaryRows: this.dictionaryRows
        });
    };


    /**
     * Save the dictionary table, updating the main array with dictionaries
     */
    saveDictionary = () => {
        /** Check if there are not required fields empty **/
        if (!this.state.dictName || this.state.dictionaryRows.length === 0) {
            this.setState({
                alertIsVisible: true
            });
        } else {
            /** Generate random id for dictionary table **/
            let dictId = parseInt(`${Math.round(Math.random() * 100)}`);

            let dictionaryComplete = {
                id: dictId,
                name: this.state.dictName,
                dictionary: this.state.dictionaryRows,
                validatingStatus: null
            };

            /** Saving dictionary table in the localStorage **/
            localStorage.setItem(`${dictId}`, JSON.stringify(dictionaryComplete));

            this.props.history.push('/')
        }
    };

    onChangeName = (e) => {
        this.setState({dictName: e.target.value});
        this.setState({
            alertIsVisible: !this.state.dictName
        });
    };

    render() {

        const {countDictRows} = this.state;

        /** Generate an array for dictionary rows **/
        let newRows = [];
        for (let i = 0; i < countDictRows; i++) {
            if (this.state.removedRowId === i) continue;
            newRows.push(<AddDictionaryContentRow key={i} id={i}
                                                  updateDictionary={this.updateDictionary}
                                                  removeDictionaryRow={this.removeDictionaryRow}
                                                  removedRowId={this.state.removedRowId}/>)
        }

        /** Set the name of the required fields in the alert **/
        let alertText = "";

        if (!this.state.dictName) {
            alertText = "Dictionary name";
        } else if (this.state.dictionaryRows.length === 0) {
            alertText = "Dictionary row";
        }


        return (
            <div className="container-fluid wrapper">

                {/* Breadcrumbs for navigation */}
                <Breadcrumb>
                    <BreadcrumbItem><Link to="/">Dictionaries</Link></BreadcrumbItem>
                    <BreadcrumbItem active>Add new dictionary</BreadcrumbItem>
                </Breadcrumb>

                {/* Alert for required fields errors */}
                <Alert color="danger" isOpen={this.state.alertIsVisible}>
                    {`Please fill in the required field "${alertText}"`}
                </Alert>

                {/* Main form to add dictionary data */}
                <Card body>
                    <Form>
                        <FormGroup>
                            <Label for="name">Dictionary name</Label>
                            <Input type="text"
                                   name="name"
                                   id="name"
                                   required
                                   placeholder="e.g. T-Shirt colors"
                                   value={this.state.dictName}
                                   onChange={this.onChangeName}/>
                        </FormGroup>

                        <div>Create dictionary, adding the new rows</div>

                        <Button outline color="primary" className="btn-margin" onClick={this.addNewRow}>+ New
                            dictionary row</Button>
                        {countDictRows > 0 && newRows}
                        {
                            countDictRows > 0 &&
                            <Button color="success"
                                    className="btn-margin"
                                    onClick={this.saveDictionary}>Save and Go Back</Button>
                        }
                    </Form>
                </Card>
            </div>
        )
    }
}

export default withRouter(AddDictionary)